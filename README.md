# JavaRAG #
JavaRAG is a library and embedded language for programming with reference attribute grammars (RAGs) in Java. JavaRAG supports several attribution mechanisms including circular and collection attributes, as well as static aspects and integration with any Java abstract syntax tree structure.

### State machine example ###
A state machine example using JavaRAG can be found at:
https://bitbucket.org/javarag/javarag-statemachine